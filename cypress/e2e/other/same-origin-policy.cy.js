/// <reference types="Cypress" />
/// <reference types="cypress-xpath" />

describe("Cypress web security", () => {
    it("Visiting 2 domains", () => {
     cy.visit('http://www.webdriveruniversity.com/')
     cy.visit('https://automationteststore.com/')
    });
    it("Navigate to automationteststore", () => {
        cy.visit('http://www.webdriveruniversity.com/')
        cy.get('#automation-test-store').invoke('removeAttr', 'target').click();
    });

    it.only("Origin command", () => {
        cy.origin("webdriveruniversity.com", () =>{
            cy.visit('/')
        })
        cy.origin("automationteststore.com", () =>{
            cy.visit('/')
        })

        cy.visit('http://www.webdriveruniversity.com/')
        cy.visit('http://selectors.webdriveruniversity.com/')
    });
  });