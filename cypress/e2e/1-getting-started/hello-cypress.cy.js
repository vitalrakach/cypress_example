/// <reference types="cypress" />
/*
Cypress.on('uncaught:exception', () => false);
it('should open web site', () => {
    cy.visit('https://www.cypress.io');
    cy.get('.footer-form > .border').click();
    cy.get('#subscribe-email').type('rakach.h4h.test@gmail.com');
    cy.get('.flex > .block').click();
    cy.get('.border-t > .border').click();
    cy.get('.leading-36px').should('have.text', 'Thank you for subscribing!');
    cy.get('.leading-36px').should('be.above', 40);
});
*/
it("should open login h4h page", () => {
  cy.visit("https://dev2-ehealth.h4h.io/ehealth-ui/");
  cy.get("#kc-page-title").should("contain.text", "Log In");
  cy.get("#username").type("vital.rakach@seavus.com");
  cy.get("#password").type("12312312312");
  cy.get("#kc-login").click();
  cy.get('[data-testid="btn-navigate-to-practitioner"]').click();
  //cy.xpath("(//div[@class='chooseRole__container--3w8SR']//a)[2]").click();
  cy.xpath("//div[@class='TableHeader__root--SRbBv']//div[1]").should(
    "contain.text",
    "Patient overview"
  );
});
