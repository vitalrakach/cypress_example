/// <reference types="Cypress" />


//Section 32
describe("Handling IFrame & Modals", () => {
  it.only("Handle webdriveruni IFrame and modal", () => {
    cy.visit("http://www.webdriveruniversity.com/");
    cy.document().should("have.property", "charset").and("eq", "UTF-8");
    cy.get("#iframe").invoke("removeAttr", "target").click({ force: true });

    cy.get("#frame").then(($iframe) => {
      const body = $iframe.contents().find("body");
      cy.wrap(body).as("iframe");
    });
    cy.get("@iframe").find("#button-find-out-more").click();
    cy.get('@iframe').find('#myModal').as('modal')

    cy.get('@modal').should(($expectedText) => {
        const text = $expectedText.text()
        expect(text).to.include('Welcome to webdriveruniversity.com we sell');
    })
    cy.get('@modal').contains('Close').click()
  });
});
