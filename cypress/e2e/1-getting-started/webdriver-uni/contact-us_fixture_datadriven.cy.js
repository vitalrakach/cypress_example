/// <reference types="Cypress" />

describe("Test Contact us form for webdriverUni", () => {
  before(function() {
    cy.fixture('example').then(function(data){
      //this.data = data
      globalThis.data = data //(use this if this.data=data does not work)
    })
  })
  it.only("Should be able to submit a successfull submission via contact us form", () => {
    cy.visit("http://www.webdriveruniversity.com/");
    cy.document().should('have.property', 'charset').and('eq', 'UTF-8');
    cy.get("#contact-us").invoke("removeAttr", "target").click({ force: true });
    cy.visit("http://www.webdriveruniversity.com/Contact-Us/contactus.html");
    cy.title().should('include', 'WebDriver | Contact Us');
    cy.url().should('include', 'contactus');
    var url = cy.url();
    cy.log(url);
    cy.get('[name="first_name"]').type(data.first_name);
    cy.get('[name="last_name"]').type(data.last_name);
    cy.get('[name="email"]').type(data.email);
    cy.get("textarea.feedback-input").type("some comments");
    cy.get('[type="submit"]').click();
    cy.get('h1').should("contain.text", "Thank You for your Message!")
  });

it("Should NOT be able to submit a successfull submission without email", () => {
    cy.visit("http://www.webdriveruniversity.com/Contact-Us/contactus.html");
    cy.get('[name="first_name"]').type("testvitfirst");
    cy.get('[name="last_name"]').type("lastName");
    cy.get("textarea.feedback-input").type("some comments");
    cy.get('[type="submit"]').click();
    cy.get('body').should("contain.text", "Error: Invalid email address")
  });
});
