describe("Validate homepage links", () => {
  it("Confirm links redirect to the correct pages", () => {
    cy.visit("http://www.webdriveruniversity.com/");
    cy.document().should("have.property", "charset").and("eq", "UTF-8");
    cy.get("#popup-alerts")
      .invoke("removeAttr", "target")
      .click({ force: true });
    cy.get("#button1").click();
    cy.on("window:alert", (str) => {
      expect(str).to.equal("I am an alert box!");
    });
  });

  it("Use STUBS for alerts", () => {
    cy.visit("http://www.webdriveruniversity.com/");
    cy.document().should("have.property", "charset").and("eq", "UTF-8");
    cy.get("#popup-alerts")
      .invoke("removeAttr", "target")
      .click({ force: true });

    const stub = cy.stub();
    cy.on("window:confirm", stub);

    cy.get("#button4")
      .click()
      .then(() => {
        expect(stub.getCall(0)).to.be.calledWith("Press a button!");
      })
      .then(() => {
        return true; //click on Cancel button
      })
      .then(() => {
        cy.get("#confirm-alert-text")
          .invoke("text")
          .should("contain", "You pressed OK!");
      });
  });

  it("Alerts 'confirm' with answer chioces NO", () => {
    cy.visit("http://www.webdriveruniversity.com/");
    cy.document().should("have.property", "charset").and("eq", "UTF-8");
    cy.get("#popup-alerts")
      .invoke("removeAttr", "target")
      .click({ force: true });
    cy.get("#button4").click();
    cy.on("window:confirm", (str) => {
      expect(str).to.equal("Press a button!"); //check that confirm alert contains text message
      return false; //click on 'Cancel' button. Use true for click on OK button
    });

    cy.get("#confirm-alert-text")
      .invoke("text")
      .should("contain", "You pressed Cancel!");
  });
});
