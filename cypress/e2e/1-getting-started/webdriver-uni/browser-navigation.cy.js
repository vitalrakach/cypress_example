/// <reference types="Cypress" />

describe("Validate homepage links", () => {
  it("Confirm links redirect to the correct pages", () => {
    cy.visit("http://www.webdriveruniversity.com/");
    cy.document().should("have.property", "charset").and("eq", "UTF-8");
    cy.get("#contact-us").invoke("removeAttr", "target").click({ force: true });
    cy.url().should("include", "contactus");
    cy.go('back');
    cy.url().should('equal', 'http://www.webdriveruniversity.com/')
    cy.reload()
    cy.reload(true) // without using cache
    cy.go('forward');
    cy.url().should("include", "contactus");
    cy.go('back');
    cy.get("#login-portal").invoke("removeAttr", "target").click({ force: true });
    cy.url().should("include", "Login-Portal");
  });
});
