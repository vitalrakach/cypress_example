/// <reference types="Cypress" />
// lecture 39
describe("Handling Date-picker", () => {
  beforeEach(() => {
    cy.visit("http://webdriveruniversity.com/");
    cy.get("#datepicker").invoke("removeAttr", "target").click({ force: true });
    cy.get(".form-control").click();
  });

  it("Select date from datepicker", () => {
    //  let date = new Date();
    //  date.setDate(date.getDate()) //get current day i.e 22
    //  cy.log(date.getDate())

    // let date2 = new Date();
    // date2.setDate(date.getDate() + 5)
    // cy.log(date2.getDate())

    var date = new Date();
    date.setDate(date.getDate() + 355); // set tomorrow as a date
    cy.log(date.toISOString()); // isostring

    var futureYear = date.getFullYear(); //year of tomorrow date
    var futureMonth = date.toLocaleString("default", { month: "long" }); // formatted month of tommorrow date
    var futureDay = date.getDate(); // day of tomorrow date

    cy.log(futureYear);
    cy.log(futureMonth);
    cy.log(futureDay);

    function selectMonthAndDay() {
      cy.get(".datepicker-dropdown")
        .find(".datepicker-switch")
        .first()
        .then((currentDate) => {
          if (!currentDate.text().includes(futureYear)) {   //if displayed year in dateoicker not equal to futureYear
            cy.get(".next").first().click();                // then click next (month) button
            cy.log("trigger next button by YEAR");
            selectMonthAndDay();                            //call the function again and again until condition is true
          }
        })
        .then(() => {
          cy.get(".datepicker-dropdown")
            .find(".datepicker-switch")
            .first()
            .then((currentDate) => {
              if (!currentDate.text().includes(futureMonth)) {
                cy.get(".next").first().click();
                cy.log("trigger next button by MONTH");
                selectMonthAndDay();
              }
            });
        })
    }


    function selectFutureDay() {
        cy.get('[class="day"]').contains(futureDay).click();
    }
    selectMonthAndDay(); //call the function of seletion year and month
    selectFutureDay(); //call the function of seletion date
  });
});
