/// <reference types="Cypress" />

//Section 32
describe("Checkboxes", () => {
  beforeEach(() => {
    cy.visit("http://www.webdriveruniversity.com/");
    cy.document().should("have.property", "charset").and("eq", "UTF-8");
    cy.get("#dropdown-checkboxes-radiobuttons")
      .invoke("removeAttr", "target")
      .click({ force: true });
  });

  it("Verify check checkboxes", () => {
    cy.get("#checkboxes > :nth-child(1) > input").check().should("be.checked"); //check and verify it is checked
  });
  it("Uncheck checked (hardcoded)", () => {
    cy.get(":nth-child(5) > input").uncheck().should("not.be.checked");
  });

  it("Find checked by input 'checked' and uncheck", () => {
    cy.get("#checkboxes").find("input").uncheck().should("not.be.checked");
  });

  it("Select multiple checkboxes", () => {
    cy.get("#checkboxes")
      .find("input[type=checkbox]")
      .check()
      .should("be.checked");
    // find element by index
    //cy.get('#checkboxes').find('input[type=checkbox]').eq(1).check().should('be.checked')
  });
});
