/// <reference types="Cypress" />


describe("Radio buttons", () => {
  beforeEach(() => {
    cy.visit("http://www.webdriveruniversity.com/");  
    cy.get("#dropdown-checkboxes-radiobuttons").invoke("removeAttr", "target").click({ force: true })
  });



    it("Verify Radio buttons", () => {
        cy.get('#radio-buttons').find("[type='radio']").first().check()
        cy.get('#radio-buttons').find("[type='radio']").eq(1).check() //Select by INDEX
    });
    

    it("Check radio button by value", () => {
      cy.get("[value='lettuce']").check().should('be.checked')
      cy.get("[value='pumpkin']").should('not.be.checked')
      cy.get('[id="radio-buttons-selected-disabled"]').find("[type='radio']").eq(1).should('be.disabled')
        
    });
  });
  