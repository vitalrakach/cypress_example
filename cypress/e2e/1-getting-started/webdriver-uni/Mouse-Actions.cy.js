/// <reference types="Cypress" />

describe("Verify AMOUSE ACTIONS", () => {
  it("Scroll mouse", () => {
    cy.visit("http://www.webdriveruniversity.com/");
    cy.get("#actions")
      .scrollIntoView()
      .invoke("removeAttr", "target")
      .click({ force: true });
  });

  it("Drag&Drop", () => {
    cy.visit("http://www.webdriveruniversity.com/");
    cy.get("#actions")
      .scrollIntoView()
      .invoke("removeAttr", "target")
      .click({ force: true });
    cy.get("#draggable").trigger("mousedown", { which: 1 }); //click and hold mouse on the center of the element
    cy.get("#droppable")
      .trigger("mousemove")
      .trigger("mouseup", { force: true });
  });
  it("Double Click", () => {
    cy.visit("http://www.webdriveruniversity.com/");
    cy.get("#actions").scrollIntoView().invoke("removeAttr", "target").click({ force: true });
    cy.get("#double-click").dblclick();
  });

  it.only("Hol down clicked element", () => {
    cy.visit("http://www.webdriveruniversity.com/");
    cy.get("#actions").scrollIntoView().invoke("removeAttr", "target").click({ force: true });
    cy.get("#click-box").trigger("mousedown", { which: 1 }).then(($element) => {
        expect($element).to.have.css('background-color', 'rgb(0, 255, 0)')
    })
  });
});
