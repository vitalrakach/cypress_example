/// <reference types="Cypress" />
// lecture 39
describe("Upload file", () => {
  beforeEach(() => {
    cy.visit("http://webdriveruniversity.com/");
    cy.get("#file-upload")
      .invoke("removeAttr", "target")
      .click({ force: true });
  });

  it.only("Upload file test1", () => {
    cy.get("#myFile").selectFile("cypress/fixtures/laptop.png");
    cy.get("#submit-button").click();
  });

  it("Click on submit without upload file", () => {
    cy.get("#submit-button").click();
    cy.on("window:alert", (str) => {
      expect(str).to.equal("You need to select a file to upload!");
    }).then(() => {
      return true; //click on OK button
    });
  });
});
