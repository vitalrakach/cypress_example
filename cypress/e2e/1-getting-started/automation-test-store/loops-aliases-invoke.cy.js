/// <reference types="Cypress" />
/// <reference types="cypress-xpath" />

describe("Alias and invokes", () => {
  it("Validate a specific hair care product", () => {
    cy.visit("https://automationteststore.com/");
    cy.get("a[href*='product/category&path=']").contains("Hair Care").click();
    cy.get(".fixed_wrapper .prdocutname")
      .eq(0)
      .invoke("text")
      .as("productThumbnail");

    //aliases should starts with @ symbol
    cy.get("@productThumbnail").its("length").should("be.gt", 5);
    cy.get("@productThumbnail").should("contain", "Seaweed Conditioner");
  });

  it("Validate a specific hair care product", () => {
    cy.visit("https://automationteststore.com/");
    cy.get(".thumbnail").as("productThumbnail").its("length").should("eq", 16);
    cy.get("@productThumbnail")
      .find(".productcart")
      .invoke("attr", "title")
      .should("include", "Add to Cart");
  });
  it.only("Calculate total of normal and sale products", () => {
    cy.visit("https://automationteststore.com/");
    cy.get(".thumbnail").as("productThumbnail");
    // cy.get('@productThumbnail').find('.oneprice').each(($el, index, $list) => {
    //     cy.log($el.text());
    // });
    cy.get(".thumbnail").find(".oneprice").invoke("text").as("itemPrice");
    cy.get(".thumbnail").find(".pricenew").invoke("text").as("saleItemPrice");
    var itemsTotal = 0;
    cy.get("@itemPrice").then(($linkText) => {
      var itemPriceTotal = 0;
      var itemPrice = $linkText.split("$");
      var i;
      for (i = 0; i < itemPrice.length; i++) {
        cy.log(itemPrice[i]);
        itemPriceTotal += Number(itemPrice[i]);
      }
      itemsTotal += itemPriceTotal;
      cy.log("Non sale items total: " + itemPriceTotal);
      cy.log("itemsTotal: " + itemsTotal);
    });
    cy.get("@saleItemPrice").then(($linkText) => {
      var saleItemPriceTotal = 0;
      var saleItemPrice = $linkText.split("$");
      var i;
      for (i = 0; i < saleItemPrice.length; i++) {
        cy.log(saleItemPrice[i]);
        saleItemPriceTotal += Number(saleItemPrice[i]);
      }
      cy.log("Sale items total: " + saleItemPriceTotal);
      itemsTotal += saleItemPriceTotal;
      cy.log("itemsTotal: " + itemsTotal);
      expect(itemsTotal).to.be.equal(625.60)
    });
  
    
  });
});
