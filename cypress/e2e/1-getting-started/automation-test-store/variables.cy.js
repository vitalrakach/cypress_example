/// <reference types="Cypress" />
/// <reference types="cypress-xpath" />

describe("Variables", () => {
  it("Click on variables", () => {
    cy.visit("https://automationteststore.com/");
    const makeupLink = cy
      .get("a[href*='product/category&path=']")
      .contains("Makeup");
    const skincareLink = cy
      .get("a[href*='product/category&path=']")
      .contains("Skincare");

    makeupLink.click();
    skincareLink.click();
  });

  it("Click on variables", () => {
    cy.visit("https://automationteststore.com/");
    const makeupLink = cy
      .get("a[href*='product/category&path=']")
      .contains("Makeup");
    makeupLink.click();
    cy.get("h1 .maintext").then(($mainText) => {
      var mainText = $mainText.text();
      cy.log("Header text: " + mainText);
      expect(mainText).to.equal("Makeup");
    });
  });

  it.only("Click on variables", () => {
    cy.visit("https://automationteststore.com/");
    cy.xpath("//a[contains(@href, 'contact')]").click();

    //Cypress commands and chaining
    cy.contains("#ContactUsFrm", "Contact Us Form")
      .find("#field_11")
      .should("contain", "First name");

    //JQuery approach
    cy.contains("#ContactUsFrm", "Contact Us Form").then((text) => {
      const firstNameText = text.find("#field_11").text();
      expect(firstNameText).to.contain("First name");
    });

    //Embedded commands (Closure)
    cy.get("#field_11").then((fnText) => {
      cy.log(fnText.text());
      cy.log(fnText);
    });
  });
});
