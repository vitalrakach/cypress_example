/// <reference types="Cypress" />
/// <reference types="cypress-xpath" />

describe("Test Contact us form for automation test store", () => {
  before(function() {
    cy.fixture('userDetails').as("user") // alias for fixture
  })
  it("Should be able to submit a successfull submission via contact us form", () => {
    cy.visit("https://automationteststore.com/");
    cy.xpath("//a[contains(@href, 'contact')]").click().then(function(linkText){  //Where itemAttributeText is custom name
      cy.log("Text of contact us link: " + linkText.text())
    });
    //cy.get(".info_links_footer > :nth-child(5) > a").click();
    cy.get("@user").then((user) => {                            //call fixture alias
      cy.get("#ContactUsFrm_first_name").type(user.first_name);
      cy.get("#ContactUsFrm_email").type(user.email);
    })
    
    cy.get("#ContactUsFrm_email").should("have.attr", "name", "email");
    cy.get("#ContactUsFrm_enquiry").type("fill some free text");
    cy.xpath("//button[@title='Submit']").click();
    cy.xpath(
      "//div[@id='maincontainer']/div[1]/div[1]/div[1]/div[1]/section[1]/p[2]"
    ).should(
      "have.text",
      "Your enquiry has been successfully sent to the store owner!"
    );
    //   cy.get('h1').should("contain.text", "Thank You for your Message!")
    console.log("Some console text");
    let date = new Date();
    cy.log(date.toString());
  });
});
