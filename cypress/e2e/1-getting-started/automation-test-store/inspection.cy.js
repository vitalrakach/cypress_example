/// <reference types="Cypress" />
/// <reference types="cypress-xpath" />

describe("Open first item of products", () => {
    it("Click on first item from product list", () => {
      cy.visit("https://automationteststore.com/");
      cy.xpath("/html//section[@id='featured']/div[@class='container-fluid']/div[@class='block_frame block_frame_featured']//a[@title='Skinsheen Bronzer Stick']").click();

    });

    it.only("Click on first item using index", () => {
        cy.visit("https://automationteststore.com/");
        var date = new Date();
        console.log(date);
        cy.get('.fixed_wrapper').find('.prdocutname').eq(0).click(); 
      });
  });